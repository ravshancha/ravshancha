
# Ismoilov Ravshanjon
### Software Engineer [Java / Spring / React / Next.js]
## `💻 Coding ...` | Uzbekistan

[![CV](https://img.shields.io/badge/CV-Download-black?style=for-the-badge&logo=readdotcv)](mailto:ismoilov.ravshanjon@gmail.com)
[![Web-site](https://img.shields.io/badge/ravshancha.uz-black?style=for-the-badge&logo=revoltdotchat)](https://ravshancha.uz)
[![GitHub](https://img.shields.io/badge/GitHub-black?style=for-the-badge&logo=github)](https://gist.github.com/ravshancha)
[![Gitlab](https://img.shields.io/badge/Gitlab-000000?style=for-the-badge&logo=Gitlab)](https://www.gitlab.com/ravshancha)
[![BitBucket](https://img.shields.io/badge/BitBucket-000000?style=for-the-badge&logo=BitBucket)](https://www.bitbucket.com/ravshancha)
[![LeetCode](https://img.shields.io/badge/LeetCode-black?style=for-the-badge&logo=leetcode)](https://gist.github.com/ravshancha)
[![Medium](https://img.shields.io/badge/Medium-black?style=for-the-badge&logo=medium)](https://gist.github.com/ravshancha)
[![Gmail](https://img.shields.io/badge/Mail-black?style=for-the-badge&logo=Gmail)](mailto:ismoilov.ravshanjon@gmail.com)
[![Telegram](https://img.shields.io/badge/Telegram-black?style=for-the-badge&logo=Telegram)](https://t.me/ravshancha)
[![Whatsapp](https://img.shields.io/badge/WhatsApp-black?style=for-the-badge&logo=Whatsapp)](https://www.wa.me/+998994370770)
[![LinkedIn](https://img.shields.io/badge/LinkedIn-black?style=for-the-badge&logo=LinkedIn)](https://www.linkedin.com/in/ravshancha)
[![Twitter](https://img.shields.io/badge/Twitter-black?style=for-the-badge&logo=Twitter)](https://twitter.com/ravshancha)
[![Facebook](https://img.shields.io/badge/Facebook-black?style=for-the-badge&logo=Facebook)](https://www.facebook.com/ravshancha)
[![Instagram](https://img.shields.io/badge/Instagram-black?style=for-the-badge&logo=Instagram)](https://www.instagram.com/ravshancha.uz)

Web developer with a high regard to interactive fast websites, clean code and advanced algorithms
> Get my act together towards catching up with the speed of development of new technologies.

[![Ismoilov Ravshanjon's GitHub stats ](https://github-readme-stats.vercel.app/api?username=ravshancha&show_icons=true&theme=dark)](https://gitlab.com/ravshancha)

[![Ismoilov Ravshanjon's GitHub stats ](https://github-readme-stats.vercel.app/api/top-langs/?username=ravshancha&theme=blue-green)](https://gitlab.com/ravshancha)

### **Language & Technologies**
![JAVA](https://img.shields.io/badge/Java-black?style=for-the-badge&logo=openjdk) 
![JAVA](https://img.shields.io/badge/Spring-black?style=for-the-badge&logo=spring) 
![Oracle](https://img.shields.io/badge/Oracle-000000?style=for-the-badge&logo=Oracle&logoColor=ff0000) 
![PostgreSQL](https://img.shields.io/badge/PostgreSQL-000000?style=for-the-badge&logo=PostgreSQL)
![Git](https://camo.githubusercontent.com/324ecb8e3920e6c4826b60f2afd553c8a1b6ea87782030de0eaa65bb8c8b2919/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f2d4769742d4630353033323f7374796c653d666f722d7468652d6261646765266c6f676f3d676974266c6f676f436f6c6f723d7768697465)
![Figma](https://img.shields.io/badge/Figma-000000?style=for-the-badge&logo=Figma) 
![HTML](https://img.shields.io/badge/HTML5-000000?style=for-the-badge&logo=HTML5) 
![CSS](https://img.shields.io/badge/CSS3-000000?style=for-the-badge&logo=CSS3) 
![JavaScript](https://img.shields.io/badge/JavaScript-000000?style=for-the-badge&logo=JavaScript) 
![SASS](https://img.shields.io/badge/SASS-000000?style=for-the-badge&logo=SASS) 
![Bootstrap](https://img.shields.io/badge/Bootstrap-000000?style=for-the-badge&logo=Bootstrap) 
![Nodejs](https://camo.githubusercontent.com/7f4931495ba3a8b88b75935ec00486ccb40d30b8d613829df0bdf86eaf2d8abb/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f2d4e6f64656a732d3433383533643f7374796c653d666f722d7468652d6261646765266c6f676f3d4e6f64652e6a73266c6f676f436f6c6f723d7768697465) 
![React.js](https://img.shields.io/badge/React-000000?style=for-the-badge&logo=React) 
![Next.js](https://img.shields.io/badge/Vue.js-000000?style=for-the-badge&logo=next.js) 
![ReactHooks](https://camo.githubusercontent.com/cb3c8adf7b1a07a0236bf33802aa7919a64df5dd3a12e4b7f5bff214fba480dd/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f2d52454143545f484f4f4b532d3332353061383f7374796c653d666f722d7468652d6261646765266c6f676f3d5245414354266c6f676f436f6c6f723d) 
![ReactRouter](https://camo.githubusercontent.com/a4ca6b71d62aa6f56199242308ccb9619737bc6d78aeb0599ba5978866e72789/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f2d52656163745f526f757465722d626c61636b3f7374796c653d666f722d7468652d6261646765266c6f676f3d72656163742d726f75746572266c6f676f436f6c6f723d6f72616e6765)
![Heroku](https://camo.githubusercontent.com/34d9487f29365780fa14138d197a71172a0e1fb8765fcb28734dcd0770f7eba7/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f2d4865726f6b752d3736344142433f7374796c653d666f722d7468652d6261646765266c6f676f3d6865726f6b75266c6f676f436f6c6f723d7768697465)
![Postman](https://camo.githubusercontent.com/cce340a49ea69f07b55fd3481df80def1dc60ee829557922bb09a31ff5a66944/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f2d504f53544d414e2d3332356461383f7374796c653d666f722d7468652d6261646765266c6f676f3d504f53544d414e266c6f676f436f6c6f723d)
### **Forums**
[![Slack](https://img.shields.io/badge/Slack-000000?style=for-the-badge&logo=Slack)](https://www.wa.me/+998994370770)
[![StackOverflow](https://img.shields.io/badge/Stack_Overflow-000000?style=for-the-badge&logo=stack-overflow)](https://www.bitbucket.com/ravshancha)
### **Projects**
[![Netlify](https://img.shields.io/badge/Netlify-000000?style=for-the-badge&logo=Netlify)](https://app.netlify.com/teams/ravshancha)
### **Presentation**
[![Youtube](https://img.shields.io/badge/Youtube-000000?style=for-the-badge&logo=Youtube)](https://www.youtube.com/@ravshancha)

# Skills
- **Languages:** `Java`, `JavaScript`
- **Web development:** `React Js`, `JQuery`, `Ajax` 
- **CSS Frameworks:** `Bootstrap`, `Bootstrap React`
- **Favorite Libraries:** `Material-ui`, `Axios`, `Redux`, `Vuex`
- **Other:** `Rest API`, `Cookie`, `Agile Scrum`

# Experience
### **Backend Developer**
**Tune Consulting**, January, 2020 - December, 2021
<br>
#### Kapital Bank - Apelsin & Market Place
- Интеграция между системами
- Разработка интернет-магазина
- Разработка административной панели для приложения
- Технологии:
![JAVA](https://img.shields.io/badge/Java-000000?style=for-the-badge&logo=Java&logoColor=ff0000)
![JAVA](https://img.shields.io/badge/spring-000000?style=for-the-badge&logo=Java&logoColor=ff0000)
 Spring (Boot 2, Security, Data), ActiveMQ, SOAP, REST, JSON, XML
  Swagger 2.0, Oracle, Redis, ElasticSearch, MongoDB


### **WEB DEVELOPER**
**Stylemix**, April, 2019 - December, 2019
<br> [Stylemix](http://stylemix.net) delivers WordPress support and maintenance services internationally. Customizes themes from [StylemixThemes](http://stylemixthemes.com)
- Installing and updating Wordpress, Themes, Plugins
- WooCommerce and payment gateway integrations/modifications 
- Developing websites under Google Page Speed (90%+)
- Cache controlling
- Customizing websites in clients' desire
- Creating front-end of websites under different criteria
- Integration Mobile App with Wordpress Website


### **IELTS MENTOR**
**Zippy Students**, October, 2018 - February 2019
- Improved students' skills from Pre-Ielts to Ielts 
- Results: IETLS band 5+

# Education

**TUIT, 2013 - 2017** <br>
**Computer Engineering**

- Founder's Scholarship Winner for 4 years
- GPA 4.31/4.5 [link](https://drive.google.com/file/d/1HVUAPpzXPC_9SYIB3hMw-8mpmKnMSK3h/view)
- The academic excellence is in “Dean’s List”.

# Certificates - [link](https://drive.google.com/file/d/1egiMKQTI_DCZ_xek-cilgFymsEM43qOp/view)

### Open Data Challenge, 2022
* [Rewarded](https://inha.uz/en/news/709/) with first place for a project called "Clean City".
* Worked with a team during two days 
* From Idea to Implementation

[//]: # (### IELTS Certificate 8.0 band score, 2020-2022)
[//]: # ()
[//]: # (* Reading 8.5)
[//]: # (* Listening 8.0)
[//]: # (* Speaking 7.5)
[//]: # (* Writing 7.5)

### Other [achievements](https://drive.google.com/file/d/1XheymE_r1Fp5WSNa37EurkkBvezRtazI/view) till 2017
* Mathematical olimpiads
* Sport competitions

# Conferences
### Perspektywy Women in Tech Summit in Poland, 2019
- The Winner of scholarship for attending [Perspektywy Women in Tech Summit](https://womenintechsummit.pl/),
the world's biggest conference for women in IT & TECH in Europe

# Projects
[avto.kapitalbank.uz ](https://avto.kapitalbank.uz) - Auto Credit Form and Loan Calculation SPA<br>
>Technologies: Vue, Vuex, Vue Resource, Pdf-make

- 1 day challenge (website was developed in 20 hours)
- Server side rendering
- Layout handling (responsive)
- Loan Calculator

<hr>